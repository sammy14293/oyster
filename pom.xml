<project xmlns="http://maven.apache.org/POM/4.0.0" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
	http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>edu.ualr</groupId>
	<artifactId>oyster</artifactId>
	<version>3.6.7</version>
	<packaging>jar</packaging>

	<name>OYSTER : Open sYSTem Entity Resolution Engine</name>
	<url>https://bitbucket.org/oysterer/oyster/wiki/Home</url>
	<scm>
		<url>https://bitbucket.org/oysterer/oyster</url>
	</scm>
	<issueManagement>
		<system>BitBucket</system>
		<url>https://bitbucket.org/oysterer/oyster/issues?status=new&amp;status=open</url>
	</issueManagement>

	<organization>
		<name>UA Little Rock ERIQ</name>
		<url>http://ualr.edu/informationquality/</url>
	</organization>
	<properties>
		<app.main.class>edu.ualr.oyster.OysterMain</app.main.class>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<jdk.version>1.8</jdk.version>
		<commons-io.version>2.6</commons-io.version>
		<commons-text.version>1.2</commons-text.version>
		<commons-lang3.version>3.7</commons-lang3.version>
		<commons-collections4.version>4.1</commons-collections4.version>
		<slf4j.version>1.7.24</slf4j.version>
		<log4j.version>1.2.17</log4j.version>
		<junit.version>4.8.2</junit.version>
	</properties>

	<dependencies>
		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
		<dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-lang3</artifactId>
		    <version>${commons-lang3.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-collections4 -->
		<dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-collections4</artifactId>
		    <version>${commons-collections4.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-collections4 -->
		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-text -->
		<dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-text</artifactId>
		    <version>1.2</version>
		</dependency>
	
		<!-- Test -->
		<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
		<dependency>
		    <groupId>commons-io</groupId>
		    <artifactId>commons-io</artifactId>
		    <version>${commons-io.version}</version>
		    <scope>test</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j.version}</version>
		    <scope>test</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12 -->
		<dependency>
		    <groupId>org.slf4j</groupId>
		    <artifactId>slf4j-log4j12</artifactId>
		    <version>${slf4j.version}</version>
		    <scope>test</scope>
		</dependency>
		<dependency> 
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId> 
			<version>${log4j.version}</version> 
		    <scope>test</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<finalName>${project.artifactId}-${project.version}</finalName>
		<plugins>
			<!-- download source code in Eclipse, best practice -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.9</version>
				<configuration>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>true</downloadJavadocs>
				</configuration>
			</plugin>

			<!-- Set a compiler level -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>${jdk.version}</source>
					<target>${jdk.version}</target>
				</configuration>
			</plugin>
			<!-- Maven JAR Plugin -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.0.2</version>
			    <executions>
			        <execution>
			            <phase>build</phase>
			            <goals>
			                <goal>single</goal>
			            </goals>
						<configuration>
							<archive>
								<index>true</index>
								<manifest>
									<Main-Class>${app.main.class}</Main-Class>
									<addClasspath>true</addClasspath>
									<classpathPrefix>lib/*</classpathPrefix>
									<classpathLayoutType>repository</classpathLayoutType>
									<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
									<addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
								</manifest>
								<manifestEntries>
									<Class-Path>lib/*</Class-Path>
								</manifestEntries>
							</archive>
						</configuration>
			        </execution>
			    </executions>
			</plugin>
			<plugin>
			    <groupId>org.apache.maven.plugins</groupId>
			    <artifactId>maven-shade-plugin</artifactId>
				<version>3.1.0</version>
			    <executions>
			        <execution>
						<phase>package</phase>
			            <goals>
			                <goal>shade</goal>
			            </goals>
			            <configuration>
							<archive>
								<index>false</index>
							</archive>
			                <shadedArtifactAttached>false</shadedArtifactAttached>
			                <transformers>
			                    <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
			                        <mainClass>${app.main.class}</mainClass>
			                </transformer>
							<transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
								<manifestEntries>
									<Main-Class>${app.main.class}</Main-Class>
									<Class-Path>lib/*</Class-Path>
									<X-Compile-Source-JDK>${jdk.version}</X-Compile-Source-JDK>
									<X-Compile-Target-JDK>${jdk.version}</X-Compile-Target-JDK>
									<ProjectName>${project.name}</ProjectName>
									<ProjectUrl>${project.url}</ProjectUrl>
									<Implementation-Version>${project.version}</Implementation-Version> 
									<Implementation-Title>${project.name}</Implementation-Title>: 
									<Implementation-Version>${project.version}</Implementation-Version>
									<Implementation-Vendor-Id>${project.groupId}</Implementation-Vendor-Id>
									<Implementation-Vendor>${project.organization.name}</Implementation-Vendor>
									<Implementation-URL>${project.url}</Implementation-URL>
								</manifestEntries>
							</transformer>
			            </transformers>
			        </configuration>
			        </execution>
			    </executions>
			</plugin>
		</plugins>
	</build>
	<description>Changes to Scoring Rule Logic and Fix Bug in Indexing Logic</description>
</project>