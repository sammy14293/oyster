This is a run definition using only the List_A, List_B, and List_C records 
referenced in the supplied TruthSet file.

The source and run scrips are contained in this folder as well as the List file
reduced to only the records referenced in the TruthSet.

The run script references the attributes file from the full run I am testing
which is in the ..\T2 folder.  You will probably need to adjust this reference

I run OYSTER from the parent of this folder ie.  CD ..\Truth and specify
the run script as

Truth\Truth.Run.xml

