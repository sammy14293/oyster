# OYSTER V4 design questions #

This contains a collection of notes and questions to be determined or researched as we create a new more flexible and extensible program strucure.

### Class Names ###

To make it easier to understand the program structure and the relationship of objects and classes it is important to implement a consistent naming standard that relates bot to the object classes and the configuration elements used to define an OYSTER run.

#### edu.ualr.oyster.association.matching classes ####

These classes are named "comparator".  
They seem to be related to the Algo= property of the <Attribute> tag in the OysterAttributes file.

The Attributes tag has two attributes.

- Item
    - Must be assigned a name of an attribute that is used to define a reference in the source file.
    - The value must match the value assigned to Attribute in the <Item> tag of the OysterSourceDescriptor file for the same attribute or OYSTER will produce the error: “##ERROR: Reference Items and Attributes do not match.”
	
- Algo
    - Assign a pre-defined matching algorithm that is to be used for comparing attribute values of this type
    - Specifying an algorithm is optional, and if it not given or a given name is not found, then a default matching algorithm is used.
    - The default matching algorithm supports many different comparators which are defined in detail in the <Term> Tag section of this document.

Not that it refers to these as "pre-defined matching algorithm" rather than a comparator and refers to "comparators" as those defined in the <Term> tag which are defined as the compareXxxx() methods in the utility classes.


