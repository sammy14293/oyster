/*
 * Copyright 2012 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.index;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import edu.ualr.oyster.data.ClusterRecord;
import edu.ualr.oyster.data.OysterIdentityRecord;
// JLTRUE removed temporarily
//import edu.ualr.oyster.utilities.ChinesePinYin;
import edu.ualr.oyster.utilities.DaitchMokotoffSoundex;
import edu.ualr.oyster.utilities.IBMAlphaCode;
import edu.ualr.oyster.utilities.NYSIISCode;
import edu.ualr.oyster.utilities.Scan;
import edu.ualr.oyster.utilities.Soundex;
import edu.ualr.oyster.utilities.ListTokenizerPV;
import edu.ualr.oyster.utilities.ListTokenizer;
import edu.ualr.oyster.utilities.MatrixTokenizer;
import edu.ualr.oyster.utilities.PlaceHolderFilter;

import org.apache.commons.lang3.StringUtils;
// JRTalburt added to support ListTokenizer and ListTokenizerPV hash functions
import org.apache.commons.text.StrTokenizer;

/**
 * An Inverted Index is a mapping structure that maps a word to its location 
 * within the data file. The goal of an inverted index is to optimize the speed 
 * of the query: find the documents where word X occurs [Zobel, J. Moffat, A. 
 * (July 2006). "Inverted Files for Text Search Engines". ACM Computing Surveys 
 * (New York: Association for Computing Machinery) 38 (2): 6]. This simple index
 * allows fast full document search of all reference items that it has read up 
 * to the point of the index query. This index will simply be a map object. The 
 * key will be the reference item (name, DOB, etc) and the value list will be a 
 * collection of sequence numbers. 
 * 
 * The this Index differs in the key that is used for a data element. The reason
 * for this new index is two fold,
 * <ul>
 * <li>One, it enables some fuzziness to be applied to the index,</li>
 * <li>Two, it allows for a better segmentation of the data set there by 
 * creating smaller sized candidate sets while still getting the most reliable 
 * candidates</li>
 * </ul>
 * It takes a record and creates an entry based on each rule in the ruleset. For
 * Example given the following:
 * 
 * Given this ruleset
 * Item="First", MatchCode="LED(80)"
 * Item="Last", MatchCode="Exact_Ignore_Case"
 * Item="SSN", MatchCode="Transpose"
 * 
 * This formula is used:
 * "Soundex(First)"+"Substring(UpperCase(Last),0,4)"+"CharsInOrder(SSN)"
 * 
 * If the Input record had the values "Phillip, Johnson, 432-43-7652", then the
 * index key value would be: "P410JOHNS22334567"
 * Where
 * "P410" is the Soundex value of "Phillip"
 * "JOHNS" is the first 5 chars in upper case of "Johnson"
 * "223344567" are the 9 alphanumeric chars of "432-43-7652" in sorted order
 * 
 * @author Eric D. Nelson
 */

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.47CC8817-13E6-2FA6-29A3-1D3DE2906C50]
// </editor-fold> 
public class TalburtZhouInvertedIndex extends InvertedIndex {

    private int slidingWindow = -1;
    
    /** The rules to be used for the ER run */
    private ArrayList<IndexRule> rules = null;
    
    /** Standard Soundex utility */
    private Soundex soundex;

    /** Standard Soundex utility */
    private DaitchMokotoffSoundex dmSoundex;

    /** IBM AlphaCode Soundex utility */
    private IBMAlphaCode alphaCode;

    /** NYSIISCode Soundex utility */
    private NYSIISCode nysiis;
    
    /** Scan Utility */
    private Scan scan;
    
    /** ListTokenizerPV utility */
    private ListTokenizerPV listtokenizerpv;
    
    /** ListTokenizer utility */
    private ListTokenizer listtokenizer;
    
    /** MatrixTokenizer utility */
    private MatrixTokenizer matrixtokenizer;
    
    /** PlaceHolderFilter utility */
    private PlaceHolderFilter placeholderfilter;
    
    /** ChinesePinYin Utility */
    // JLTRUE removed temporarily
    // private ChinesePinYin pinyin;
    
    /**
     * Creates a new instance of <code>TalburtZhouInvertedIndex</code>.
     */
    public TalburtZhouInvertedIndex () {
        super();
        
        soundex = new Soundex();
        dmSoundex = new DaitchMokotoffSoundex();
        alphaCode = new IBMAlphaCode();
        nysiis = new NYSIISCode();
        scan = new Scan();
        listtokenizerpv = new ListTokenizerPV();
        listtokenizer = new ListTokenizer();
        matrixtokenizer = new MatrixTokenizer();
        placeholderfilter = new PlaceHolderFilter();
        // JLTRUE removed temporarily
        // pinyin= new ChinesePinYin();
    }

    public int getSlidingWindow() {
        return slidingWindow;
    }

    public void setSlidingWindow(int slidingWindow) {
        this.slidingWindow = slidingWindow;
    }

    public ArrayList<IndexRule> getRules() {
        return rules;
    }

    public void setRules(ArrayList<IndexRule> rules) {
        this.rules = rules;
    }
    
    /**
     * Add an entry to the <code>TalburtZhouInvertedIndex</code>.
     * @param obj the entry to be added.
     */
    @Override
    public void addEntry(Object obj) {
        if (obj != null) {
            OysterIdentityRecord oir = (OysterIdentityRecord) obj;

            // get the RefID
            String value = oir.get("@RefID");
            Set<String> hashes = getHash(oir, rules);
            for (Iterator<String> it = hashes.iterator(); it.hasNext();) {
                String hash = it.next();
                Set<String> s = index.get(hash);

                if (s == null) {
                    s = new LinkedHashSet<String>();
                }

                String[] values = value.split("[|]");
                s.addAll(Arrays.asList(values));
                index.put(hash, s);
            }
        } else {
            System.out.println("Null obj insertion into index");
        }
    }

    /**
     * Add an entry to the <code>TalburtZhouInvertedIndex</code>.
     * @param refID the refID to set the reference items to.
     * @param obj the entry to be added.
     */
    @Override
    public void addEntry(String refID, Object obj) {
        if (obj != null) {
            OysterIdentityRecord oir = (OysterIdentityRecord) obj;
            Set<String> hashes = getHash(oir, rules);

            for (Iterator<String> it = hashes.iterator(); it.hasNext();) {
                String hash = it.next();
                Set<String> s = index.get(hash);

                if (s == null) {
                    s = new LinkedHashSet<String>();
                }

                s.add(refID);
                index.put(hash, s);
            }
        } else {
            System.out.println("Null obj insertion into index");
        }
    }
    
    /**
     * Associates the specified value with the specified key in this <code>
     * TalburtZhouInvertedIndex</code>. If the <code>TalburtZhouInvertedIndex</code>
     * previously contained a mapping for this key, the old value is replaced by
     * the specified value. 
     * @param key the key with which the specified value is to be associated.
     * @param o the value to be associated with the specified key.
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public void add(String key, Object o){
        Set<String> s = index.get(key.trim());
        Set<String> set = (Set<String>) o;
        if (s != null) {
            s.addAll(set);
            
            index.put(key.trim(), s);
        }
        else {
            index.put(key.trim(), set);
        }
    }
    
    /**
     * Associates the specified value with the specified key in this <code>
     * TalburtZhouInvertedIndex</code>. If the <code>TalburtZhouInvertedIndex</code>
     * previously contained a mapping for this key, the old value is replaced by
     * the specified value. 
     * @param key the key with which the specified value is to be associated.
     * @param value the value to be associated with the specified key.
     */
    @Override
    public void setEntryAt(Object key, String value) {
        OysterIdentityRecord oir = (OysterIdentityRecord) key;
        
        if (oir != null) {
            Set<String> hashes = getHash(oir, rules);

            for (Iterator<String> it = hashes.iterator(); it.hasNext();) {
                String hash = it.next();
                Set<String> s = index.get(hash);

                if (s == null) {
                    s = new LinkedHashSet<String>();
                }

                if (!s.contains(value) && !hash.equals("") && hash != null) {
                    s.add(value);
                    index.put(hash, s);
                }
            }
        } else {
            System.out.println("Null obj insertion into index");
        }
    }

    /**
     * Removes the mapping for this key from this <code>TalburtZhouInvertedIndex
     * </code> if it is present.
     * @param obj the Object to be removed.
     */
    @Override
    public void removeEntry (Object obj) {
        String key = (String) obj;
        index.remove(key);
    }

    /**
     * Remove the refID from the Object in the <code>TalburtZhouInvertedIndex</code>.
     * @param obj the Object to be searched.
     * @param refID the refID to be removed from the Object.
     */
    @Override
    public void removeEntry(Object obj, String refID) {
        OysterIdentityRecord oir = (OysterIdentityRecord) obj;
        
        if (oir != null) {
            Set<String> hashes = getHash(oir, rules);

            for (Iterator<String> it = hashes.iterator(); it.hasNext();) {
                String hash = it.next();
                Set<String> s = index.get(hash);

                if (s != null) {
                    if (s.contains(refID)) {
                        s.remove(refID);

                    if (s.isEmpty()) {
                            Set<String> set = this.index.remove(hash);
                            set = null;
                        } else {
                            this.index.put(hash, s);
                        }
                    }
                }
            }
        }
    }

    /**
     * 
     * @param clusterRecord
     * @param lcrd
     * @return 
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public Set<String> getCandidateList(ClusterRecord clusterRecord, Map<Integer, ArrayList<String>> lcrd, boolean mergedList) {
        Set<String> candidates = new HashSet<String>();
        Map<String, Integer> sort = new LinkedHashMap<String, Integer>();
        Set<String> hashes = new LinkedHashSet<String>();
        
        
        if (mergedList) {
            for (int i = 0; i < clusterRecord.getSize(); i++){
                hashes.addAll(getHash(clusterRecord.getOysterIdentityRecord(i), rules));
            }
        } else {
            hashes.addAll(getHash(clusterRecord.getMergedRecord(), rules));
        }

        for (Iterator<String> it = hashes.iterator(); it.hasNext();) {
            String hash = it.next();
            Set<String> s = index.get(hash);
            
            if (s != null) {
                candidates.addAll(s);

                for (Iterator<String> it2 = s.iterator(); it2.hasNext();) {
                    String key = it2.next();

                    int value = 0;
                    if (sort.containsKey(key)) {
                        value = sort.get(key);
                    }
                    value++;
                    sort.put(key, value);
                }
            }
        }

        int value = 0;
        if (candidateList.containsKey(candidates.size())) {
            value = candidateList.get(candidates.size());
        }
        value++;
        candidateList.put(candidates.size(), value);
        
        // if more than 100 candidates only return the top 1/3 sorted based on most occuring
        if (candidates.size() > 100){
            TreeMap<Integer, Set<String>> sort2 = new TreeMap<Integer, Set<String>>();
            
            for(Iterator<Entry<String, Integer>> it = sort.entrySet().iterator(); it.hasNext();){
                Entry<String, Integer> entry = it.next();
                value = entry.getValue();
                
                Set<String> s2 = sort2.get(value);
                
                if (s2 == null) {
                    s2 = new LinkedHashSet<String>();
                }
                
                s2.add(entry.getKey());
                sort2.put(value, s2);
            }
            
            int check = candidates.size() / 3;
            if (check > 100) {
                check = 100;
            }
            
            candidates.clear();
            
            for (int i = sort2.lastKey(); i >= 0; i--){
                Set<String> s2 = sort2.get(i);
                
                if (s2 != null) {
                    candidates.addAll(s2);
                }
                
                if (candidates.size() > check) {
                    break;
                }
            }
        }

        return candidates;
    }

    private Set<String> getHash(OysterIdentityRecord oir, ArrayList<IndexRule> irs){
        Set<String> hashes = new LinkedHashSet<String>();
        Set<String> ruleHashes = new LinkedHashSet<String>();
        Set<String> temp = new LinkedHashSet<String>();
        ArrayList<String> termHashes = new ArrayList<String>();
        for (Iterator<IndexRule> it = irs.iterator(); it.hasNext();) {
            IndexRule ir = it.next();
            ruleHashes.clear();
            ruleHashes.add("");
            for (Iterator<String> it2 = ir.getSegments().keySet().iterator(); it2.hasNext();) {
                String attribute = it2.next();
                String hashCode = ir.getSegments().get(attribute); 
                String token = oir.get(attribute);
            	temp.clear();
                if (token != null && !token.trim().isEmpty()){
                    termHashes = decodeMethodSignature(token, hashCode);
                    if (termHashes.size() > 0) {
                    	for (Iterator<String> itx = ruleHashes.iterator(); itx.hasNext();){
                    		String prevHash = itx.next();
                    		for (int j=0; j<termHashes.size(); j++){
                    			String termHash = termHashes.get(j);
                    			if (!termHash.isEmpty()) temp.add(prevHash+termHash);
                    		}
                    	}
                    }

                } 
                ruleHashes.clear();
                ruleHashes.addAll(temp);
            }
           
            hashes.addAll(ruleHashes);
        }    
        return hashes;
    }
    
    // This method was changed from returning a String to returning a String[] array
    // The change was made to accomodate the ListTokenizer and ListTokenizerPV classes
    // that return multiple hash tokens
    public ArrayList<String> decodeMethodSignature(String token, String hashCode){
        ArrayList<String> result = new ArrayList<String>();
        result.add("");
        String matchType = "", direction = "", charType = "", upperCase = "", order = "";
        // JRTalburt parameter values for ListTokenizerPV function
        char listDelim=',', pairDelim=':';
        String placeholderValues ="";
        // JRTalburt parameter values for ListTokenizer function
        int minimumLength = 2;
        String excludedTokens = "";        
        String temp ="";
        int length = 0;
        // Get the arguments of the Hash function if it hash any
        // set matchType to name of the Hash Function
        if (hashCode.toUpperCase(Locale.US).startsWith("SOUNDEX")){
            matchType = "Soundex";
        } else if (hashCode.toUpperCase(Locale.US).startsWith("DMSOUNDEX")){
            matchType = "DMSoundex";
        } else if (hashCode.toUpperCase(Locale.US).startsWith("IBMALPHACODE")){
            matchType = "IBMAlphaCode";
        } else if (hashCode.toUpperCase(Locale.US).startsWith("NYSIIS")){
            matchType = "NYSIIS";
        } else if (hashCode.toUpperCase(Locale.US).startsWith("SCAN(")){
            matchType = hashCode.trim().substring(5, hashCode.length()-1);
            String [] args = matchType.split("[,]");
            for (int i = 0; i < args.length; i++){
                switch (i) {
                    case 0:
                        if (args[i].trim().equalsIgnoreCase("LR") || args[i].trim().equalsIgnoreCase("RL")){
                            direction = args[i].trim();
                        }
                        break;
                    case 1:
					if (args[i].trim().equalsIgnoreCase("ALL") || args[i].trim().equalsIgnoreCase("NONBLANK") || args[i].trim().equalsIgnoreCase("ALPHA")
					            || args[i].trim().equalsIgnoreCase("LETTER") || args[i].trim().equalsIgnoreCase("DIGIT") || args[i].trim().equalsIgnoreCase("CHINESEDIGIT")
					            || args[i].trim().equalsIgnoreCase("CHINESEALPHA") || args[i].trim().equalsIgnoreCase("CHINESELETTER") || args[i].trim().equalsIgnoreCase("CHINESE")) {
                            charType = args[i].trim();
                        }
                        break;
                    case 2:
                        length = Integer.parseInt(args[i].trim());
                        break;
                    case 3:
                        if (args[i].trim().equalsIgnoreCase("ToUpper") || args[i].trim().equalsIgnoreCase("KeepCase")){
                            upperCase = args[i].trim();
                        }
                        break;
                    case 4:
                        if (args[i].trim().equalsIgnoreCase("SameOrder") || args[i].trim().equalsIgnoreCase("L2HKeepDup") ||
                            args[i].trim().equalsIgnoreCase("L2HDropDup")){
                            order = args[i].trim();
                        }
                        break;
                    default: //FIXME: Should throw an error here but for now just a message
                        System.err.println("Invalid Argument List in SCAN method");
                }
            }
            matchType = "Scan";
        } else if (hashCode.toUpperCase(Locale.US).startsWith("LISTTOKENIZER(")){
            matchType = hashCode.trim().substring(14, hashCode.length()-1);
            StrTokenizer stoken = new StrTokenizer(matchType);
       	 	stoken.setDelimiterChar(',');
       	 	stoken.setQuoteChar('\'');
       	 	stoken.setIgnoredChar(' ');
       	 	String [] args = stoken.getTokenArray();
	       	// Assume first string in array is the list delimiter character
           	 if(args.length > 0 && (args[0].trim()).length()>0) {
           		 listDelim = (args[0].trim()).charAt(0);
           	 	} // otherwise the default value of listDelim will be used
           	 // Assume second string in array is the minimum length to index
           	 if(args.length > 1 && (args[1].trim()).length()>0) {
           		 try{
        			 minimumLength = Integer.parseInt(args[1].trim());
        		 } catch (Exception e) {
        			 // if conversion fails the default value of overLapThreshold will be used
        		 }
           	 } // otherwise the default value of pairDelim will be used
           	 // Assume third string in the array is the list of excluded values
       	 	if(args.length > 2) excludedTokens = args[2].trim();
            matchType = "ListTokenizer";
    	}   else if (hashCode.toUpperCase(Locale.US).startsWith("LISTTOKENIZERPV(")){
            matchType = hashCode.trim().substring(16, hashCode.length()-1);
            StrTokenizer stoken = new StrTokenizer(matchType);
       	 	stoken.setDelimiterChar(',');
       	 	stoken.setQuoteChar('\'');
       	 	stoken.setIgnoredChar(' ');
       	 	String [] args = stoken.getTokenArray();
	       	 // Assume first string in array is the listDelim character
           	 if(args.length > 0 && (args[0].trim()).length()>0) {
           		 listDelim = (args[0].trim()).charAt(0);
           	 } // otherwise the default value of listDelim will be used
           	 // Assume second string in array is the pairDelim character
           	 if(args.length > 1 && (args[1].trim()).length()>0) {
           		 pairDelim = (args[1].trim()).charAt(0);
           	 } // otherwise the default value of pairDelim will be used
           	 // Assume third string in the array is the list of placeholder values
       	 	if(args.length > 2) placeholderValues = args[2].trim();
            matchType = "ListTokenizerPV";
        }   else if (hashCode.toUpperCase(Locale.US).startsWith("MATRIXTOKENIZER(")){
            matchType = hashCode.trim().substring(16, hashCode.length()-1);
            StrTokenizer stoken = new StrTokenizer(matchType);
       	 	stoken.setDelimiterChar(',');
       	 	stoken.setQuoteChar('\'');
       	 	stoken.setIgnoredChar(' ');
       	 	String [] args = stoken.getTokenArray();
           	 // Assume first string in array is the minimum length to index
           	 if(args.length > 0 && (args[0].trim()).length()>0) {
           		 try{
        			 minimumLength = Integer.parseInt(args[0].trim());
        		 } catch (Exception e) {
        			 // if conversion fails the default value of minimum length is used
        		 }
           	 } // If second parameter not given, default value is used
           	 // Assume second string in the array is the list of excluded values
       	 	if(args.length > 1) excludedTokens = args[1].trim();
            matchType = "MatrixTokenizer";
        } else if (hashCode.toUpperCase(Locale.US).startsWith("PLACEHOLDERFILTER(")){
            matchType = hashCode.trim().substring(18, hashCode.length()-1);
            placeholderValues = matchType;
            matchType = "PlaceHolderFilter";	
        } else if (hashCode.toUpperCase(Locale.US).startsWith("CHINESEPINYIN")){
        	matchType = "ChinesePinYin";
        }
        
        // Stop here if the hash code is not recognized
        if (StringUtils.isBlank(matchType)) {
        	System.out.println("Terminating run: Invalid Index Hash:" + hashCode);
        	throw new IllegalArgumentException("Invalid Index Hash:" + hashCode); 
        }
        
        // Call the Hash Function
        if (matchType.equalsIgnoreCase("Soundex")){
        	temp = soundex.getSoundex(token);
            result.set(0, temp );
        } else if (matchType.equalsIgnoreCase("DMSoundex")){
        	temp = dmSoundex.getDMSoundex(token)[0];
        	result.set(0, temp);
        } else if (matchType.equalsIgnoreCase("IBMAlphaCode")){
        	temp = alphaCode.getIBMAlphaCode(token);
        	result.set(0, temp);
        } else if (matchType.equalsIgnoreCase("NYSIIS")){
        	temp = nysiis.getNYSIISCode(token);
        	result.set(0, temp);
        } else if (matchType.equalsIgnoreCase("Scan")){
        	temp = scan.getScan(token, direction, charType, length, upperCase, order);
        	result.set(0, temp);
// JLTRUE removed temporarily
//        } else if (matchType.equalsIgnoreCase("ChinesePinYin")) {
//            result = pinyin.getChinesePinYin(token);
        } else if(matchType.equalsIgnoreCase("ListTokenizerPV")){
        	String [] hashValues = listtokenizerpv.getHashKeys(listDelim, pairDelim, placeholderValues, token);
        	if(hashValues.length > 0){
        		for(int j=0; j < hashValues.length; j++){
        			result.add(j, hashValues[j]);
        		}
        	}
        } else if(matchType.equalsIgnoreCase("ListTokenizer")){
        	String [] hashValues = listtokenizer.getHashKeys(listDelim, minimumLength, excludedTokens, token);
        	if(hashValues.length > 0){
        		for(int j=0; j < hashValues.length; j++){
        			result.add(j, hashValues[j]);
        		}
        	}
        } else if(matchType.equalsIgnoreCase("MatrixTokenizer")){
        	String [] hashValues = matrixtokenizer.getHashKeys(minimumLength, excludedTokens, token);
        	if(hashValues.length > 0){
        		for(int j=0; j < hashValues.length; j++){
        			result.add(j, hashValues[j]);
        		}
        	}
        } else if(matchType.equalsIgnoreCase("PlaceHolderFilter")){
        	temp = placeholderfilter.getPlaceHolder(placeholderValues, token);
        	result.set(0, temp);
        	} 
        return result;
    }
}