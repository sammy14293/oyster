package edu.ualr.oyster.utilities;

/**
 * This is a new data class to hold both the agreement weight and disagreement weight returned from the scoring rule weight table
 * It creates a data structure containing two double values for storage in the weight table.
 * The first value is the agreement weight and second is the disagreement weight
 * To be compatible with previous versions using only the agreement weight, the disagreement weight can be set to null.
 * <ul>
 * <li>Two double values</li>
 * <li>Second value may be null </li>
 * </ul>
 * @author John R. Talburt
 */

public class OysterWeightPair {
	
	double agreeWgt;
	double disagreeWgt;
	
	public void setAgreeWgt (double wgt){
		agreeWgt = wgt;
	}
	
	public void setDisagreeWgt (double wgt){
		disagreeWgt = wgt;
	}
	
	public double getAgreeWgt (){
		return agreeWgt;
	}
	
	public double getDisagreeWgt (){
		return disagreeWgt;
	}

}
