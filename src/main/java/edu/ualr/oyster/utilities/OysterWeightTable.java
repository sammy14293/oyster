/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import edu.ualr.oyster.utilities.OysterWeightPair;

import edu.ualr.oyster.formatter.ErrorFormatter;

/**
 * This class is used to find the agreement weight for certain value.
 * Each line of the text file specified by the WgtTable must have two, table-separated values.
 * The first value of the attribute defined by the Item attribute of the <Term>. 
 * The second value in each line of the text file is an integer value containing only decimal digits with an optional prefix plus symbol or a minus prefix symbol in the case of negative values, e.g. "100", "+100", or "-100".
 * The integer value represents the agreement weight for the corresponding attribute value given as the first value.
 * <ul>
 * <li>Two column tab delimited</li>
 * <li>Any comments must be preceded by !! </li>
 * </ul>
 * @author Cheng Chen
 * Revised by John Talburt 2018-11-04 to include a disagreement weight in addition to the agreement weigh
 * for each token. The weight table was changed from 
 * LinkedHashMap <String, Double> to
 * LinkedHashMap <String, OysterWeightPair> where OysterWeightPair is a new data class for an ordered
 * pair of Double values. The first Double is the agreement weight, the second is the disagreement weight
 * To be compatible with previously created table, the disagreement weight is optional (null)
 */

public class OysterWeightTable {

    /** The weight table */
    private Map <String, OysterWeightPair> weightTable;
    
    private boolean hasDisagreementWeight;
    
    public boolean containsDisagreementWeights () {
    	if (hasDisagreementWeight) return true;
    	else return false;
    }


    /**
     * Creates a new instance of <code>OysterNickNameTable</code>
     * @param filename the data file to be loaded
     */
    public OysterWeightTable (String filename) {
	    weightTable = new LinkedHashMap <String, OysterWeightPair>();
        load(filename);
    }

    /**
     * Loads the data file into the nickname table
     * @param filename the file to be loaded
     */
    private void load(String filename){
        int count = 0;
        String read;
        String [] text;
        BufferedReader infile = null;
        
        try{
            File file = new File(filename);
//            System.out.println("Loading " + file.getName());
            infile = new BufferedReader(new FileReader(file));
            hasDisagreementWeight = false;
            while((read = infile.readLine()) != null){
                if (!read.startsWith("!!")) {
                    text = read.split("[\t]");
                    String value=text[0].toLowerCase(Locale.US);
                    double weight=Double.parseDouble(text[1]);
                    OysterWeightPair pair = new OysterWeightPair();
                    pair.setAgreeWgt(weight);
                    if(text.length==3) {
                    	hasDisagreementWeight = true;
                    	weight = Double.parseDouble(text[2]);
                    	pair.setDisagreeWgt(weight);
                    } else {
                    	pair.setDisagreeWgt(0.0);
                    }
                    weightTable.put(value, pair);
                    count++;
                }
            }
            
//            System.out.println(count + " elements loaded.\n");
        } catch(IOException ex){
            Logger.getLogger(OysterWeightTable.class.getName()).log(Level.WARNING, ErrorFormatter.format(ex), ex);
        } finally {
            try {
                if (infile != null) {
                    infile.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(OysterWeightTable.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
            }
        }
    }

    /**
     * Returns the Weight Table for this <code>OysterWeightTable</code>
     * @return the Weight Table
     */
    public Map <String, OysterWeightPair> getWeightTable () {
        return weightTable;
    }
   
    /**  
     * Returns the weight pair from the weight table or null if not found
     * @param token
     * @return weight pair
     */
    public OysterWeightPair getWeightPair (String token) {
    	return weightTable.get(token.toLowerCase(Locale.US));
    }
    
    /**  
     * Returns the weight pair from the weight table or null if not found
     * @param token
     * @return weight pair
     */
    public boolean usesDisagreement () {
    	return hasDisagreementWeight;
    } 

    /**
     * Sets the Weight Table for this <code>OysterWeightTable</code>
     * @param wightTable the nickname table to be set
     */
    public void setWeightTable (Map <String, OysterWeightPair> weightTable) {
        this.weightTable = weightTable;
    }
}

