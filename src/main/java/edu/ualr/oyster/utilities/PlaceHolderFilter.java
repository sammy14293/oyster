/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.utilities;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;
/**
 * PlaceHolderFilter is an indexing and DataPrep function designed to filter
 * placeholder values before making a comparison. 
 * PlaceHolderFilter takes a string representing a list of placeholder values and
 * an input value. If an input value matches one of the placeholder values, the
 * input value is replaced by an empty string (""), otherwise the input value is
 * returned unchanged. Both the placeholder values and the input value ingnore case
 * when performing the lookup.
 * The PlaceHolderFilter takes one control parameter
 * 1) The list of placeholder value given a single string with the placeholder
 *    values separated by a pipe (|) delimiter
 */
public class PlaceHolderFilter {
	
	// Class variables
    LinkedHashSet<String> placeHolderValues = new LinkedHashSet<String>();
    
    public PlaceHolderFilter() {
    }
    
    private void populatePlaceHolderValues(String ph){
    	ph = ph.toUpperCase(Locale.US);
    	String [] myArray = ph.split("[|]");
    	placeHolderValues.addAll(Arrays.asList(myArray));
    }
    
    private boolean isPlaceHolder(String item){
    	if(placeHolderValues.contains(item.toUpperCase(Locale.US))) {
    		return true; 
    	} else {
    		return false;
    	}
    }
    
	public String getPlaceHolder(String phs, String inStr){
	    populatePlaceHolderValues(phs);
	    String result;
		if (isPlaceHolder(inStr)) {
			result = "";
		} else {
			result = inStr;
		}
		return result;
   }
	
	public static void main(String[] args){
 	    String str1 = "UNKNOWN|UNK|NA|?";
 	    String str2 = "Unknown";
 	    PlaceHolderFilter phf = new PlaceHolderFilter();
 	    System.out.println(str1);
 	    String result = phf.getPlaceHolder(str1, str2);
 	 	System.out.println("Result = ["+result+"]");
 	 	 
	}

}
