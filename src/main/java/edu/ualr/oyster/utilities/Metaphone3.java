/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Metaphone3.java
 * 
 * 
 * Created on Apr 29, 2012 8:38:38 AM
 * @author Eric D. Nelson
 */
public class Metaphone3 {
    /**
     * Creates a new instance of Metaphone3
     */
    public Metaphone3(){
    }

    public String getMetaphone(String s) {
        String result = "", temp = "", firstLetter = "";
        char c;

        try {
            if (s != null && s.trim().length() > 0) {
                temp = s.toUpperCase(Locale.US);

                // 1. Standardize the string by removing all punctuations and spaces
                temp = temp.replaceAll("\\p{Punct}", "");
                temp = temp.replaceAll("\\p{Space}", "");

                if (temp.trim().length() > 0) {
                }
            }
        } catch (RuntimeException ex) {
            Logger.getLogger(Metaphone3.class.getName()).log(Level.SEVERE, "s:" + s + " temp:" + temp + " result:" + result, ex);
            result = null;
        }
        return result;
    }
    
    /**
     * This method compares two String using the Metaphone3. If the 
     * Strings produce the same encoding then the Strings are considered a 
     * match and true is returned.
     * @param s input source String
     * @param t input target String
     * @return true if the source and target are considered a match, otherwise
     * false.
     */
    public boolean compareMetaphone(String s, String t){
        String sMetaphone = getMetaphone(s);
        String tMetaphone = getMetaphone(t);
        
        if(sMetaphone == null) return false;
        sMetaphone = sMetaphone.trim();
        if(sMetaphone.equals("")) return false;
        if(tMetaphone == null) return false;
        tMetaphone = tMetaphone.trim();
        if(tMetaphone.equals("")) return false;
        if(sMetaphone.equals(tMetaphone)) {
        	return true; 
        } else {
        	return false;
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

}
