package edu.ualr.oyster.utilities;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;

public class ListTokenizerPV {
	
	// Class variables
    char listDelimiter;
    char pairDelimiter;
    LinkedHashSet<String> placeHolderValues = new LinkedHashSet<String>();
    LinkedHashSet<String> hashKeys = new LinkedHashSet<String>();
    
    public ListTokenizerPV() {
    }
    
    private void populatePlaceHolderValues(String ph){
    	// Convert all letters to upper case
    	ph = ph.toUpperCase(Locale.US);
    	String [] myArray = ph.split("[|]");
    	placeHolderValues.addAll(Arrays.asList(myArray));
    }
    
    private boolean isPlaceHolder(String item){
    	if(placeHolderValues.contains(item.toUpperCase(Locale.US))) {
    		return true; 
    	} else {
    		return false;
    	}
    }
    
	public String [] getHashKeys(char ld, char pd, String phs, String inStr){
		listDelimiter = ld;
		pairDelimiter = pd;
	    populatePlaceHolderValues(phs);
		String [] listRaw = inStr.split("["+Character.toString(listDelimiter)+"]");
		hashKeys.clear();
		// Start loop to check for valid, non-duplicate property-value pairs
		String property="";
		String value="";
		for(int i = 0; i< listRaw.length; i++) {
			String pair = (listRaw[i].trim()).toUpperCase(Locale.US); 
		    if(pair.length()> 0) {
		      		int splitPoint = pair.indexOf(pairDelimiter);
		      		if(splitPoint > 0){
		      			property = (pair.substring(0, splitPoint)).trim();
		      			if(splitPoint < pair.length()){
		      				value = (pair.substring(splitPoint+1)).trim();
		      				if(value.length() > 0 && !isPlaceHolder(value)){
		      					pair = property + pairDelimiter + value;
		      					hashKeys.add(pair);
		      					}
		      				}
		      			}
		      		}
		    	}
		return hashKeys.toArray(new String[0]);
	    }
	
	public static void main(String[] args){
 	    char d = '|';
 	    char p = ':';
 	    String str1 = "UNKNOW|UNK|NA|?";
 		String str2 = "a:12|b: 23 |c:34 |d:  UNK|e:56|f:?|UNK:NA";
 	    String str3 = "UNKNOW:?|d:UNK|f:?|g:||UNK:NA| h||k m ";
 	    ListTokenizerPV ltpv = new ListTokenizerPV();
 	    System.out.println(str2);
		System.out.println("result size = "+ltpv.getHashKeys(d, p, str1, str2).length);
 	    String [] result = ltpv.getHashKeys(d, p, str1, str2);
 	    if (result.length > 0){
 	 	    for (int j=0; j<result.length; j++){
 	 	    	System.out.println(result[j]);
 	 	    }
 	    } else System.out.println("No tokens");
 	    System.out.println(str3);
 	    result = ltpv.getHashKeys(d, p, str1, str3);
 	    if (result.length > 0){
 	 	    for (int j=0; j<result.length; j++){
 	 	    	System.out.println(result[j]);
 	 	    }
 	    } else System.out.println("No tokens");
	}

}
