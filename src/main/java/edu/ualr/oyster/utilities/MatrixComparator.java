/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.utilities;

import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.Locale;
import java.util.HashSet;
import java.util.Iterator;
import java.io.*;


/**
 * Matrix comparator takes two strings, parses the strings into a list of tokens according to
 * a regular expression given by the user. After removing any excluded tokens given by the user, 
 * all pairs of tokens between the two lists are compared using using the Levenshtein edit distance 
 * function. After scoring all pairs, the highest scores for each row and
 * column are averaged. If the average is greater than or equal to the user specified
 * threshold value, the function returns true, otherwise false. Matrix takes three
 * control parameters 
 * 1) Threshold value between 0.00 and 1.00,
 * 2) Regular expression for tokenizing the strings
 * 3) Optional list of excluded tokens
 */
public class MatrixComparator {
	
    float matrixScore;
    float thresholdValue;
    boolean tokenTableLoaded = false;
    LinkedHashSet<String> excludedTokens = new LinkedHashSet<String>();
    OysterEditDistance led = new OysterEditDistance();
    // HashSet added for weight analysis
    //HashSet<String> words = new HashSet<String>();
    
	public MatrixComparator () {
		
	}
	
	public float getMatrixScore () {
		return matrixScore;		
	}
	
    private void populateExcludedTokens(String ts){
    	String [] myArray = ts.split("[|]");
    	excludedTokens.clear();
    	if (myArray.length>0){
    		for (int k=0; k<myArray.length; k++){
    			String token = myArray[k].trim();
    			token = token.replaceAll("\\W", "").trim();
    			if (token.length()>0) excludedTokens.add(token.toUpperCase(Locale.US));
    		}
    	}
    	tokenTableLoaded = true;
    	return;
    }
    
    private boolean isExcludedToken(String item){
    	if(excludedTokens.contains(item)) return true; return false;
    }
    
    private ArrayList<String> getTokens(String inStr) {
    	inStr = inStr.toUpperCase(Locale.US);
    	String [] temp = inStr.split("[\\W]");
    	ArrayList<String> cleanList = new ArrayList<String>();   
    	if (temp.length == 0) return cleanList;    	
    	for (int k=0; k<temp.length; k++) {
    		String token = temp[k];		
    		if(token.length()>0) {
    			if(!isExcludedToken(token)) cleanList.add(token);
    		}
    	}
    	return cleanList;
    }
	
	public boolean computeMatrix(float th, String ex, String s1, String s2) {
		//Set class variable values
		thresholdValue = th;
		// System.out.println(th+" "+reg+" "+ex+" "+s1+" "+s2);
		if (!tokenTableLoaded) populateExcludedTokens(ex);
		ArrayList<String> list1 = getTokens(s1);
		//System.out.println(list1);
		int len1 = list1.size();
		ArrayList<String> list2 = getTokens(s2);
		//System.out.println(list2);
		int len2 = list2.size();
		// System.out.println(len1+" "+len2);
		// in case either or both strings are empty return false
		if (len1<1 || len2<1) return false;
		// if both strings have values, generate matrix of LED values
		float[][] matrix = new float[len1][len2];
	    int trials = Math.min(len1,len2);
	    float matrixMax = 0.0f;
	    // Collect all words with exact agreement between strings
	    //words.clear();
	    for(int i = 0; i<len1; i++) {
			for(int j = 0; j < len2; j++) {
				led.computeDistance(list1.get(i),list2.get(j));
				float dist = led.computeNormalizedScore();
				matrix[i][j] = dist;
				if (matrixMax < dist) matrixMax = dist;
				//if (dist==1.0) words.add(list1.get(i));
			}
	    }
	    // if greatest distance less than threshold, then return false
	    if (matrixMax < thresholdValue) return false;
	    float matrixTotal = 0.0f;
	    // start process the average the largest value in each row and column
	    for (int k=0; k<trials; k++){
	    	// search for largest value in the matrix
	    	float maxValue = 0.0f;
	    	// remember cell of largest values
	    	int savei = 0;
	    	int savej = 0;
	    	// start search
	    	search_loop:
	    	for (int i=0; i<len1; i++){
	    		for (int j=0; j<len2; j++){
	    			if (matrix[i][j]>maxValue){
	    				maxValue = matrix[i][j];
	    				savei = i;
	    				savej = j;
	    				// if max value is 1.0 no need to search further
	    				if (maxValue==1.0f) break search_loop;
	    			}			
	    		}
	    	}
	    	// only continue searching matrix if the last maximum value found is positive		    	
	    	if (maxValue<=0.0) break;
    		matrixTotal = matrixTotal + maxValue;
	    	// remove this row and column from further consideration
	    	for (int i=0; i<len1; i++){
	    		matrix[i][savej] = -1.0f;
	    	}
	    	for (int j=0; j<len2; j++){
	    		matrix[savei][j] = -1.0f;
	    	}
	    	//for (int i=0; i<len1; i++){
	    		//for (int j=0; j<len2; j++){  			
	    			//if (j==0) System.out.format("%n%.6f ", matrix[i][j]); else System.out.format("%.6f ", matrix[i][j]);	
	    		//}
	    	//}
	    }
	    // calculate matrix average
	    matrixScore = matrixTotal/(float)trials;    
	    // Dump agreement words for analysis
	    //if (!words.isEmpty()) {	
	    	//try {
	    		//File file = new File(".\\MatrixComparator\\Output\\words.txt");
	    		//BufferedWriter output = new BufferedWriter(new FileWriter(file, true));
			    //Iterator<String> iterator = words.iterator();
			    //while (iterator.hasNext()){
			    	//output.write(iterator.next()+"\t"+matrixScore+"\r\n");
			    //}
			   //output.close();
	    	//} catch (IOException e) {
	    		//System.out.println("Something went wrong with word output");
	    	//}
	    //}
	    
	    if (matrixScore >= thresholdValue) return true; else return false;
 
}


public static void main(String[] args) {
    MatrixComparator ed = new MatrixComparator(); 
    String s = "John.*A Doe,123 Oak, Little Rock, AR 72212, (501)793-7616";
	    String t = "Doe, Jon&Mary, 123 N Oak, Little Roc, Ark 72214";
	    boolean result = ed.computeMatrix(0.80f, "Little|Rock|Ar", s,t);
	    System.out.print(" Score: "+result);
	}
		
}

