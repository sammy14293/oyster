/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;
/**
 * ListTokenizer is the indexing function designed to support the ListOverlap Comparator
 * The ListTokenizer takes a string representing a list of items and splits it into 
 * separate list times according to the list delimiter character given by the user. 
 * After splitting, the list of items is filtered by two criteria.
 * If the user has provided a minimum item length, then any tokens shorter than
 * the minimum item length are removed.
 * Secondly, if the user has given a list of excluded item values, then any item in
 * the input list matching one of the excluded item values is removed.
 * The ListTokenizer takes three control parameters
 * 1) The list delimiter character used to split the string into list items
 *    If not delimiter character is specified, the comma is used by default
 * 2) An integer value specifying the minimum length of a token to index
 * 3) Optional list of excluded items
 */

public class ListTokenizer {
	
	// Class variables
    char listDelimiter;
    int minimumLength;
    String excludedTokens;
    LinkedHashSet<String> excludedValues = new LinkedHashSet<String>();
    LinkedHashSet<String> hashKeys = new LinkedHashSet<String>();
    
    public ListTokenizer() {
    }
    
    private void populateExcludedValues(String ph){
    	if ((ph.trim()).length() > 0){
        	String [] myArray = ph.split("[|]");
        	for (int j=0; j<myArray.length; j++)
        	{
        		String exclude = myArray[j].trim();
        		if (exclude.length() > 0) excludedValues.add(exclude.toUpperCase(Locale.US));	
        	}
    	}
    }
    
    private boolean isExcludedValue(String item){
    	if(excludedValues.contains(item.toUpperCase(Locale.US))) {
    		return true; 
    	} else {
    		return false;
    	}
    }
    
	public String [] getHashKeys(char ld, int min, String ex, String inStr){
		listDelimiter= ld;
		minimumLength = min;
	    populateExcludedValues(ex);
		String [] listRaw = inStr.split("["+Character.toString(listDelimiter)+"]");
		hashKeys.clear();
		// Start loop to check for valid, non-duplicate property-value pairs
		for(int i = 0; i< listRaw.length; i++) {
			String token = (listRaw[i].trim()).toUpperCase(Locale.US); 
		    if(token.length()> 0) {
		      	if(token.length() > minimumLength && !isExcludedValue(token)) hashKeys.add(token);
		      	}
		    }
		return hashKeys.toArray(new String[0]);
	    }
	
	public static void main(String[] args){
 	    char ld = ',';
 	    int min = 2;
 	    String ex = "XX|PVC";
 		String inStr = "John, XX, Talburt, Pvc, Pipe";
 	    ListTokenizer ltpv = new ListTokenizer();
 	    System.out.println(inStr);
		System.out.println("result size = "+ltpv.getHashKeys(ld, min, ex, inStr).length);
 	    String [] result = ltpv.getHashKeys(ld, min, ex, inStr);
 	    if (result.length > 0){
 	 	    for (int j=0; j<result.length; j++){
 	 	    	System.out.println(result[j]);
 	 	    }
 	    } else System.out.println("No tokens");
	}


}
