/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.util.Arrays;
import java.util.Locale;
import java.util.LinkedHashSet;

/**
 * 
 * ListOverlapPV takes as input two lists of property-value pairs represented by strings, 
 * parses each string into a list of pairs by splitting on the list delimiter character, then
 * splitting each pair by the pair delimiter. After removing empty items, duplicate items, items with
 * missing property values, and items where the property value is a placeholder,
 * the function determines the number of pairs in common
 * between the two lists. The overlap ratio is calculated as the number of common items
 * divided by the number of items in the longer of the two lists. If this ratio is 
 * greater than or equal to the threshold value, the function returns true, otherwise false.
 *
 */
public class ListOverlapPV {

	// Class variables
    LinkedHashSet<String> placeHolderValues = new LinkedHashSet<String>();
    double threshold;
    char listDelimiter;
    char pairDelimiter;
    
    /**
     * Creates a new instance of <code>ListOverlap</code>
     */
    public ListOverlapPV() {
    }
    
    private void populatePlaceHolderValues(String ph){
    	String [] myArray = ph.split("[|]");
    	placeHolderValues.addAll(Arrays.asList(myArray));
    }
    
    private boolean isPlaceHolder(String item){
    	if(placeHolderValues.contains(item)) return true; return false;
    }

    /**
     * 
     * @param t minimum overlap threshold
     * @param d char used as list delimiter
     * @param s1 String containing list 1
     * @param s2 String containing list 2
     * @return result
     */
    public boolean compareListOverlapPV(double t, char ld, char pd, String phs, String arg1, String arg2){
        // set class values
    	threshold = t;
        listDelimiter = ld;
        pairDelimiter = pd;
        populatePlaceHolderValues(phs);
    	
    	double result = 0.0;
        String [] list1;
        String [] list2;   
	    int len1, len2, max, overlap;

	    list1 = convertStringToListPV(arg1); 
	    len1 = list1.length;
	    list2 = convertStringToListPV(arg2);
	    len2 = list2.length;
	    
	    if (len1>len2) max = len1; else max = len2;
	    LinkedHashSet<String> hashSet = new LinkedHashSet<String>();
	    hashSet.addAll(Arrays.asList(list1));
	    hashSet.retainAll(Arrays.asList(list2));
	    overlap = hashSet.size();	    
	    if (max>0) result = (double) overlap/(double) max; else result = 0.0;
	    if (result >= t) return true; else return false;    
    }
    
 
    
    private String [] convertStringToListPV(String inStr){

    	String [] listRaw = inStr.split("["+Character.toString(listDelimiter)+"]");
	    LinkedHashSet<String> hashSet = new LinkedHashSet<String>();
	    // Start loop to check for valid, non-duplicate property-value pairs
	    String property="";
	    String value="";
	    for(int i = 0; i< listRaw.length; i++) {
	      	String pair = (listRaw[i].trim()).toUpperCase(Locale.US);      	 
	      	if(pair.length()> 0) {
	      		int splitPoint = pair.indexOf(pairDelimiter);
	      		if(splitPoint > 0){
	      			property = (pair.substring(0, splitPoint)).trim();
	      			if(splitPoint < pair.length()){
	      				value = (pair.substring(splitPoint+1)).trim();
	      				if(value.length() > 0 && !isPlaceHolder(value)){
	      					pair = property + pairDelimiter + value;
	      					hashSet.add(pair);
	      				}
	      			}
	      		}
	      	}
	    }
	    String [] listClean= hashSet.toArray(new String[0]);
	    return listClean;
    }
   
	     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
 	    char d = '|';
 	    char p = ':';
 	    String str1 = "UNKNOW|UNK|NA|?";
 		String str2 = "a:12|b:23|c:34|d:UNK|e:56|f:?|UNK:NA";
 	    String str3 = "a:12|UNKNOW:?|c:34|c:34|d:UNK|f:?|g:||UNK:NA|g:23| h||k|m ";
 	    boolean result;
 	    double thresh = 0.50;
 	    ListOverlapPV over = new ListOverlapPV();
 	    result = over.compareListOverlapPV(thresh, d, p, str1, str2, str3);
        System.out.println(result);
    } 
}


