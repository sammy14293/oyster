/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;

/**
 * MatrixTokenizer is the indexing function designed to support the MatrixComparator
 * The MatrixTokenizer takes a string and splits it into tokens delimited by blanks
 * and any other delimiting characters provided provided by the user.  
 * Each resulting token is cleaned by removing any non-alphanumeric characters and 
 * changing all letters to upper case.
 * After splitting, the list of tokens is filtered by two criteria.
 * If the user has provided a minimum token length, then any tokens shorter than
 * the minimum token length are removed.
 * Secondly, if the user has given a list of excluded token values, then any token
 * matching one of the excluded token values is removed. Excluded tokens are also 
 * cleaned by removing non-alphanumeric characters and upper casing all letters.
 * The MatrixTokenizer takes three control parameters
 * 1) String of delimiter characters to be used to split the string into tokens
 *    Note: The delimiter characters are not returned as part of the token
 * 2) An integer value specifying the minimum length of a token to index
 * 3) Optional list of excluded tokens
 * Modification: 2018-04-03 Added function to remove all non alphanumeric characters
 * from index and exclusion tokens. Also fixed bug for minimum length to index.
 */
public class MatrixTokenizer {
	// Class variables
    String listDelimiters;
    int minimumLength;
    String excludedTokens;
    LinkedHashSet<String> excludedValues = new LinkedHashSet<String>();
    LinkedHashSet<String> hashKeys = new LinkedHashSet<String>();
    
    public MatrixTokenizer() {
    }
    
    private void populateExcludedValues(String ph){
    	// Nothing to do if placeholder list is empty
    	if ((ph.trim()).length() > 0){
        	String [] myArray = ph.split("[|]");
        	for (int j=0; j<myArray.length; j++)
        	{
        		String exclude = myArray[j].trim();
        		// Remove non-alphanumeric characters from excluded values
            	exclude = exclude.replaceAll("\\W", "").trim();
            	
            	
            	
            	// Change letters in excluded values to upper case
        		if (exclude.length() > 0) excludedValues.add(exclude.toUpperCase(Locale.US));	
        	}
    	}
    }
    
    private boolean isExcludedValue(String item){
    	if(excludedValues.contains(item)) return true; return false;
    }
    
	public String [] getHashKeys(int min, String ex, String inStr){
		// minimum length of a cleaned token to index
		minimumLength = min;
	    populateExcludedValues(ex);
		String [] listRaw = inStr.split("[\\W]");
		hashKeys.clear();
		// Start loop to add tokens to hashKey set
		for(int i = 0; i< listRaw.length; i++) {
			// Change letters to uppercase
			String token = (listRaw[i].trim()).toUpperCase(Locale.US); 
			// System.out.println(token);
			// Remove all non-alphanumeric characters
        	//token = token.replaceAll("\\W", "").trim();
		    if(token.length()> 0) {
		    	// Check if token minimum length and not in excluded value list
		      	if(token.length() >= minimumLength && !isExcludedValue(token)) hashKeys.add(token);
		      	}
		    }
		return hashKeys.toArray(new String[0]);
	    }
	
	public static void main(String[] args){
 	    int min = 2;
 	    String ex = "CASE|String";
 		String inStr = "String Tokenizer: Test Case, J.R. Talburt (501)352-2436";
 	    MatrixTokenizer mt = new MatrixTokenizer();
 	    System.out.println(inStr);
 	    System.out.println("Min = ["+min+"]"); 
 	    System.out.println("Exclude = ["+ex+"]"); 	    
 	    String [] result = mt.getHashKeys(min, ex, inStr);
 	    System.out.println("result size = "+result.length);
 	    if (result.length > 0){
 	 	    for (int j=0; j<result.length; j++){
 	 	    	System.out.println(result[j]);
 	 	    }
 	    } else System.out.println("No tokens");
	}


}
