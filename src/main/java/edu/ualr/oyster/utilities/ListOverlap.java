/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.utilities;

import java.util.Arrays;
import java.util.Locale;
import java.util.LinkedHashSet;
/**
 * 
 * ListOverlap takes two lists represented as strings, parses the strings into list items
 * by splitting on the list delimiter character. After empty items and duplicate items
 * are removed from both list, the function determines the number of items in common
 * between the two list. The overlap ratio is calculated as the number of common items
 * divided by the number of items in the longer of the two lists. If this ratio is 
 * greater than or equal to the threshold value, the function returns true, otherwise false.
 *
 */
public class ListOverlap {
    /**
     * Creates a new instance of <code>ListOverlap</code>
     */
    public ListOverlap () {
    }

    /**
     * 
     * @param t minimum overlap threshold between 0.00 and 1.00
     * @param d char used as list delimiter
     * @param s1 String containing list 1
     * @param s2 String containing list 2
     * @return result
     */
    public boolean compareListOverlap(double t, char d, String s1, String s2){
        double result = 0.0;
        String [] list1;
        String [] list2;   
	    int len1, len2, max, over;

	    list1 = convertStringToList(d, s1);
	    len1 = list1.length;
	    list2 = convertStringToList(d, s2);
	    len2 = list2.length;
	    
	    // max is the number of items in the longest list
	    if (len1>len2) max = len1; else max = len2;
	    LinkedHashSet<String> hashSet = new LinkedHashSet<String>();
	    // Two operations find the intersection between the two lists
	    hashSet.addAll(Arrays.asList(list1));
	    hashSet.retainAll(Arrays.asList(list2));
	    over = hashSet.size();	    
	    if (max>0) result = (double) over/(double) max; else result = 0.0;
	    if (result >= t) return true; else return false;    
    }
    
    // Method to parse string into list items
    private String [] convertStringToList(char d, String inStr){
        String delim = Character.toString(d);
	    String [] listRaw = inStr.split("["+delim+"]");
	    LinkedHashSet<String> hashSet = new LinkedHashSet<String>();

	    int k = 0;
	    for(int i = 0; i< listRaw.length; i++) {
	    	// trim blanks and upper case letters in each list item
	      	String item = (listRaw[i].trim()).toUpperCase(Locale.US);
	      	// omit empty items
	    	if(item.length()> 0) {
	    		// HashSet will eliminate duplicates within the list
	    		hashSet.add(item);
	    		k++;
	    	}
	    }
	    String [] listClean= hashSet.toArray(new String[0]);
	    return listClean;
    }
	     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	String str1 = "A|B|C|D|E";
 	    String str2 = "A|B|C";
 	    boolean result;
 	    double thresh = 0.60;
 	    char delimiter = '|';
 	    ListOverlap over = new ListOverlap();
 	    result = over.compareListOverlap(thresh, delimiter, str1, str2);
        System.out.println(result);
    } 
}

