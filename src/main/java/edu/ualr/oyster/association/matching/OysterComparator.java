/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.association.matching;

import java.util.ArrayList;
import java.util.Locale;

/**
 * This is the base comparator class from which all other comparator class should
 * extend from. 
 * 
 * @author Eric D. Nelson
 */

public class OysterComparator {

    protected boolean debug = false;

    /** Holds the matchCodes for the comparator */
    protected ArrayList<String> matchCodes;
    
    /* Creates a new instance of <code>OysterComparator</code>.
     */
    public OysterComparator () {
        matchCodes = new ArrayList<String>();
        String [] arr = {"EXACT", "EXACT_IGNORE_CASE","MISSING","EXACTORBOTHMISSING","EXACTOREITHERMISSING"};
        for (int i = 0; i < arr.length; i++){
            matchCodes.add(arr[i]);
            matchCodes.add("~"+arr[i]);
        }
    }

    /**
     * Returns whether the <code>OysterComparator</code> is in debug mode.
     * @return true if the <code>OysterComparator</code> is in debug mode, otherwise false.
     */
    public boolean isDebug () {
        return debug;
    }

    /**
     * Enables/disables debug mode for the <code>OysterComparator</code>.
     * @param debug true to enable debug mode, false to disable it.
     */
    public void setDebug (boolean debug) {
        this.debug = debug;
    }

    /**
     * Returns whether the match code is a valid match code for this <code>OysterComparator</code>.
     * @param s match code to be checked.
     * @return true if the <code>OysterComparator</code> is in debug mode, otherwise false.
     */
    public boolean isMatchCode(String s){
        // FIXME: Need to add a resource bundle for Locale oriented data
        return matchCodes.contains(s.toUpperCase(Locale.US));
    }
    
    /**
     * This method compares the source string to the target string based on this 
     * Comparators comparison parameters. If a comparison is found will return
     * the appropriate match code string.
     * @param s source String.
     * @param t target String.
     * @param matchType the type of match to preform.
     * @return the match code found.
     */
    public String compare (String s, String t, String matchType) {
        String result, tempMatchType = matchType;
        boolean not = false;
        
        // Check for NOT operator
        if (matchType.toUpperCase(Locale.US).startsWith("~")){
            matchType = matchType.substring(1);
            not = true;
        }
        
        //======================================================================
        //======================================================================
        if (!not) {
            if (matchType.equalsIgnoreCase("Missing")) {
            	result = "X";
            	if (s==null) s = "";
            	if (t==null) t = "";
            	s=s.trim();
            	t=t.trim();
            	if(s.isEmpty()) result = tempMatchType;
            	if(t.isEmpty()) result = tempMatchType;           	
            } else if (matchType.equalsIgnoreCase("Exact") && (s.equals(t)
                    && s != null && !(s.trim()).isEmpty()
                    && t != null && !(t.trim()).isEmpty())) {
                result = tempMatchType;
            } else if (matchType.equalsIgnoreCase("Exact_Ignore_Case") && (s.equalsIgnoreCase(t)
                    && s != null && !(s.trim()).isEmpty()
                    && t != null && !(t.trim()).isEmpty())) {
                result = tempMatchType;
            } else if (matchType.equalsIgnoreCase("ExactOrBothMissing")) {
            	result = "X";
            	if (s==null) s="";
            	if (t==null) t="";
        		s = s.trim();
        		t = t.trim();
        		if (!s.isEmpty() && !t.isEmpty() && s.equals(t)) result=tempMatchType;
        		if (s.isEmpty() && t.isEmpty()) result=tempMatchType;
            } else if (matchType.equalsIgnoreCase("ExactOrEitherMissing")) {
            	result = "X";
            	if (s==null) s="";
            	if (t==null) t="";
        		s = s.trim();
        		t = t.trim();
        		if (!s.isEmpty() && !t.isEmpty() && s.equals(t)) result=tempMatchType;
        		if (s.isEmpty() || t.isEmpty()) result=tempMatchType;    		
            } else {
                result = getMatchCode(s, t, tempMatchType);
            }
        } else {
            if (matchType.equalsIgnoreCase("Missing")) { 
            	result = tempMatchType;
            	if (s==null) s = "";
            	if (t==null) t = "";
            	s=s.trim();
            	t=t.trim();
            	if(s.isEmpty()) result = "X";
            	if(t.isEmpty()) result = "X"; 
            } else if (matchType.equalsIgnoreCase("Exact") && !(s.equals(t)
                    && s != null && !(s.trim()).isEmpty()
                    && t != null && !(t.trim()).isEmpty())) {
                result = tempMatchType;
            } else if (matchType.equalsIgnoreCase("Exact_Ignore_Case") && !(s.equalsIgnoreCase(t)
                    && s != null && !(s.trim()).isEmpty()
                    && t != null && !(t.trim()).isEmpty())) {
                result = tempMatchType;
            } else if (matchType.equalsIgnoreCase("ExactOrBothMissing")) {
            	result = tempMatchType;
            	if (s==null) s="";
            	if (t==null) t="";
        		s = s.trim();
        		t = t.trim();
        		if (!s.isEmpty() && !t.isEmpty() && s.equals(t)) result="X";
        		if (s.isEmpty() && t.isEmpty()) result="X";
            } else if (matchType.equalsIgnoreCase("ExactOrEitherMissing")) {
            	result = tempMatchType;
            	if (s==null) s="";
            	if (t==null) t="";
        		s = s.trim();
        		t = t.trim();
        		if (!s.isEmpty() && !t.isEmpty() && s.equals(t)) result = "X";
        		if (s.isEmpty() || t.isEmpty()) result = "X";    
            } else {
                result = getMatchCode(s, t, tempMatchType);
            }
        }
        return result.toUpperCase();
    }

    /**
     * Returns the match code for this <code>OysterComparator</code>.
     * @param s source String.
     * @param t target String.
     * @param matchType the type of match to preform.
     * @return the match code.
     */
    public String getMatchCode (String s, String t, String matchType) {
        return "X";
    }
    
    public final String getAllAvailableMatchCodes(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(matchCodes);
        return sb.toString();
    }
    
    @Override
    /**
     * Returns a String representation of the <code>OysterComparator</code> object
     * @return a String
     */
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        
        return sb.toString();
    }
}

