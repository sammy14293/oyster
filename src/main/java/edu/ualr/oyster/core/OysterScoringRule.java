/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.core;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This class represents the Oyster Rule.
 * 
 * @author Cheng Chen
 */

public class OysterScoringRule {
	
	/** The attribute term and the associated matchCode */
	private Set<ScoringRuleTerm> termList;
	
	/** The name of this rule */
	private String ruleIdentifer = null;
	
	private double matchScore;
	
	private double reviewScore;
	
	/** */
	private boolean debug = false;
	
	/**
	 * Creates a new instance of <code>OysterRule</code>.
	 */
	public OysterScoringRule() {
		termList = new LinkedHashSet<ScoringRuleTerm>();
	}
	
	/**
	 * Returns the Rule Identifier for this <code>OysterRule</code>.
	 * 
	 * @return the Rule name.
	 */
	public String getRuleIdentifer() {
		return ruleIdentifer;
	}
	
	/**
	 * Sets the Rule Identifier for this <code>OysterRule</code>.
	 * 
	 * @param ruleIdentifer
	 *              the rule identifier to be set.
	 */
	public void setRuleIdentifer(String ruleIdentifer) {
		this.ruleIdentifer = ruleIdentifer;
	}
	
	/**
	 * Returns the Term List for this <code>OysterRule</code>.
	 * 
	 * @return the Term List.
	 */
	public Set<ScoringRuleTerm> getTermList() {
		return termList;
	}
	
	/**
	 * Sets the Term List for this <code>OysterRule</code>.
	 * 
	 * @param termList
	 */
	public void setTermList(Set<ScoringRuleTerm> termList) {
		this.termList = termList;
	}
	
	/**
	 * Adds the specified item and match result to this map, increasing its
	 * size by one.
	 * 
	 * @param rt
	 */
	public void insertTerm(ScoringRuleTerm rt) {
		termList.add(rt);
	}
	
	/**
	 * Adds the specified item and match result to this map, increasing its
	 * size by one.
	 * 
	 * @param item
	 * @param similarity
	 * @param agreeWeight
	 * @param disagreeWeight
	 */
	public void insertTerm(String item, String similarity, int agreeWeight, int disagreeWeight) {
		ScoringRuleTerm srt = new ScoringRuleTerm();
		srt.setItem(item);
		srt.setSimilarity(similarity);
		srt.setAgreewgt(agreeWeight);
		srt.setDisagreewgt(disagreeWeight);
		termList.add(srt);
	}
	
	/**
	 * Adds the specified item and match result to this map, increasing its
	 * size by one.
	 * 
	 * @param item
	 * @param dataprep
	 * @param similarity
	 * @param agreeWeight
	 * @param disagreeWeight
	 */
	public void insertTerm(String item, String dataprep, String similarity, int agreeWeight, int disagreeWeight) {
		ScoringRuleTerm srt = new ScoringRuleTerm();
		srt.setItem(item);
		srt.setSimilarity(similarity);
		srt.setDataprep(dataprep);
		srt.setAgreewgt(agreeWeight);
		srt.setDisagreewgt(disagreeWeight);
		termList.add(srt);
	}
	
	public String getTermltemName(int index) {
		return null;
	}
	
	public ArrayList<String> getListOfMatchCodes(int index) {
		return null;
	}
	
	/**
	 * Returns whether the <code>OysterRule</code> is in debug mode.
	 * 
	 * @return true if the <code>OysterRule</code> is in debug mode, otherwise
	 *         false.
	 */
	public boolean isDebug() {
		return debug;
	}
	
	/**
	 * Enables/disables debug mode for the <code>OysterRule</code>.
	 * 
	 * @param debug
	 *              true to enable debug mode, false to disable it.
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	/**
	 * Returns a string representation of the <code>OysterRule</code>.
	 * 
	 * @return a string representation of this object.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getName());
		sb.append("[ruleIdentifer=");
		sb.append(this.ruleIdentifer != null ? this.ruleIdentifer : "");
		sb.append(", termList=");
		sb.append(this.termList != null ? this.termList : "");
		sb.append("]");
		return sb.toString();
	}
	
	public double getMatchScore() {
		return matchScore;
	}
	
	public void setMatchScore(double matchScore) {
		this.matchScore = matchScore;
	}
	
	public double getReviewScore() {
		return reviewScore;
	}
	
	public void setReviewScore(double reviewScore) {
		this.reviewScore = reviewScore;
	}
	
	public Boolean isEmpty(){
		return this.termList.size()==0;
	}
}
