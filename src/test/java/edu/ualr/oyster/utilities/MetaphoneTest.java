package edu.ualr.oyster.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class MetaphoneTest {

	/*
	 * The constructor is empty so no tests required.
	 */

	@Test
	public void testGetMetaphone() {
		assertEquals("STFN", new Metaphone().getMetaphone("Steffen"));
		assertEquals("STPN", new Metaphone().getMetaphone("Stephen"));
		assertEquals("FLS", new Metaphone().getMetaphone("FALLIS"));
		assertEquals("FLS", new Metaphone().getMetaphone("VALLIS"));
		assertEquals("KLN", new Metaphone().getMetaphone("KLINE"));
		assertEquals("KLN", new Metaphone().getMetaphone("CLINE"));
	}

	@Test
	public void testCompareMetaphone() {
		assertFalse(new Metaphone().compareMetaphone("Steffen","Stephen"));
		assertTrue(new Metaphone().compareMetaphone("CLINE","CLINE"));
		assertTrue(new Metaphone().compareMetaphone("CLINE","KLINE"));
		assertFalse(new Metaphone().compareMetaphone("CLINE","FALLIS"));
	}

}
