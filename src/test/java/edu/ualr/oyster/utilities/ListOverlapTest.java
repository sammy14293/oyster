package edu.ualr.oyster.utilities;

import static org.junit.Assert.*;
import org.junit.Test;

public class ListOverlapTest {
	
	ListOverlap test = new ListOverlap();

	@Test
	public void test1() {
		assertEquals(true,test.compareListOverlap(0.50, ',', "a,b,c,d,e,f", "a,,b,c,d,g,h"));
	}
	
	@Test
	public void test2() {
		assertEquals(false,test.compareListOverlap(0.50, ',', "a,b,c,d,e,f", "a,,b,,,g,h"));
	}
	@Test
	public void test3() {
		assertEquals(false,test.compareListOverlap(0.50, ';', "a,b,c,d,e,f", "a,,b,,,g,h"));
	}
	
	@Test
	public void test4() {
		assertEquals(true,test.compareListOverlap(0.50, '|', "a|b|c|d|e|f", "a||b|c|d|g|h"));
	}
	
	@Test
	public void test5() {
		assertEquals(true,test.compareListOverlap(0.50, ':', "a:b:c:d:e:f", "a::b:c:d:g:h"));
	}

	@Test
	public void test6() {
		assertEquals(false,test.compareListOverlap(0.80, ',', "a,b,c,d,e,f", "a,,b,c,d,g,h"));
	}
	
	@Test
	public void test7() {
		assertEquals(false,test.compareListOverlap(0.80, ',', "a,b,c,d,e,f", "a,,b,,,g,h"));
	}
	@Test
	public void test8() {
		assertEquals(false,test.compareListOverlap(0.80, ';', "a,b,c,d,e,f", "a,,b,,,g,h"));
	}
	
	@Test
	public void test9() {
		assertEquals(false,test.compareListOverlap(0.80, '|', "a|b|c|d|e|f", "a||b|c|d|g|h"));
	}
	
	@Test
	public void test10() {
		assertEquals(false,test.compareListOverlap(0.80, ':', "a:b:c:d:e:f", "a::b:c:d:g:h"));
	}
}
