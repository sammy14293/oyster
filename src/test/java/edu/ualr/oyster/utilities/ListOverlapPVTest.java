package edu.ualr.oyster.utilities;

import static org.junit.Assert.*;
import org.junit.Test;

public class ListOverlapPVTest {
	
	    String str1 = "UNKNOW|UNK|NA|?";
		String str2 = "a:12|b:23|c:34|d:UNK|e:56|f:?|UNK:NA";
	    String str3 = "a:12|UNKNOW:?|c:34|c:34|d:UNK|f:?|g:||UNK:NA|g:23| h||k|m ";
	    
		ListOverlapPV test = new ListOverlapPV();

		@Test
		public void test1() {
			assertEquals(false,test.compareListOverlapPV(0.60, '|', ':', "UNKNOW|UNK|NA|?", "a:12|b:23|c:34|d:UNK|e:56|f:?|UNK:NA","a:12|UNKNOW:?|c:34|c:34|d:UNK|f:?|g:||UNK:NA|g:23| h||k|m " ));
		}
		
		@Test
		public void test2() {
			assertEquals(true,test.compareListOverlapPV(0.50, '|', ':', "UNKNOW|UNK|NA|?", "a:12|b:23|c:34|d:UNK|e:56|f:?|UNK:NA","a:12|UNKNOW:?|c:34|c:34|d:UNK|f:?|g:||UNK:NA|g:23| h||k|m " ));
		}


}
